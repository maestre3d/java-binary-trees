import java.util.Scanner;

public class Master {
    Scanner sc = new Scanner(System.in);

    public Master() {
        int opc = 0, element;
        String name;
        BinaryTree bTree = new BinaryTree();

        do {
            try {
                System.out.println("--------------------------");
                System.out.println("Select option\n1. Add node\n2. In Order traversing\n3. Pre Order traversing\n4. Post Order traversing\n5. Search Node\n6. Delete Node\n7+. Out");
                opc = Integer.parseInt(sc.nextLine());
                switch (opc) {
                    case 1:
                        System.out.println("--------------------------");
                        System.out.println("Node's number: ");
                        element = Integer.parseInt(sc.nextLine());
                        System.out.println("Node's name: ");
                        name = sc.nextLine();
                        bTree.insertNode(element, name);
                        break;
                    case 2:
                        // If not empty
                        if (!bTree.isEmpty()) {
                            System.out.println("----------  IN ORDER  ----------------");
                            bTree.inOrder(bTree.root);
                        } else {
                            System.out.println("Tree's empty.");
                        }
                        break;

                    case 3:
                        // If not empty
                        if (!bTree.isEmpty()) {
                            System.out.println("------------ PRE ORDER --------------");
                            bTree.preOrder(bTree.root);
                        } else {
                            System.out.println("Tree's empty.");
                        }
                        break;

                    case 4:
                        // If not empty
                        if (!bTree.isEmpty()) {
                            System.out.println("-------------  POST ORDER  -------------");
                            bTree.postOrder(bTree.root);
                        } else {
                            System.out.println("Tree's empty.");
                        }
                        break;
                    case 5:
                        if (!bTree.isEmpty()) {
                            System.out.println("Node's number: ");
                            element = Integer.parseInt(sc.nextLine());
                            // If not found statement
                            if (bTree.searchNode(element) == null) {
                                System.out.println(element + " not found.");
                            } else {
                                System.out.println("Node Found.\n--------DATA-----------\n");
                                System.out.println(bTree.searchNode(element));
                            }
                        } else {
                            System.out.println("Tree's empty.");
                        }
                        break;
                    case 6:
                        if (!bTree.isEmpty()) {
                            System.out.println("Node's number: ");
                            element = Integer.parseInt(sc.nextLine());
                            // If not found statement
                            if (bTree.deleteNode(element) == false) {
                                System.out.println(element + " not found.");
                            } else {
                                System.out.println("Node deleted.");
                            }
                        } else {
                            System.out.println("Tree's empty.");
                        }
                        break;
                    default:
                        System.exit(0);
                        break;
                }
            } catch (NumberFormatException ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        } while (opc != 2);
    }
}
