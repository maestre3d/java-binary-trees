public class TreeNode {
    // Prop
    int data;
    String name;
    TreeNode leftChild, rightChild;

    public TreeNode(int d, String nam) {
        // Create a node with no childs
        this.data = d;
        this.name = nam;
        this.rightChild = null;
        this.leftChild = null;
    }

    // Show all obj props
    public String toString() {
        return name + "'s data: " + data;
    }
}
