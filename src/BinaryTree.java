
public class BinaryTree {
    // If null, tree is empty
    TreeNode root;

    public BinaryTree() {
        root = null;
    }

    // Insert node method
    public void insertNode(int data, String name) {
        TreeNode node = new TreeNode(data, name);
        // If root is null, then the new node is root
        if ( root == null ) {
            root = node;
        } else {
            // Aux aims to root
            TreeNode aux = root;
            //  Father aims null
            TreeNode father;

            while ( true ) {
                father = aux;

                // If this data is less than aux's data, then go to left child
                if (data < aux.data) {
                    aux = aux.leftChild;
                    // Look for a free space place
                    if (aux == null) {
                        father.leftChild = node;
                        return;
                    }
                } else {
                    // If higher or equal, then insert into right child
                    aux = aux.rightChild;
                    if (aux == null) {
                        father.rightChild = node;
                        return;
                    }
                }
            }
        }
    }

    // Check if tree is empty
    public boolean isEmpty() {
        return root == null;
    }

    // TreeNode r = root
    public void inOrder(TreeNode r) {
        // If not empty
        if (r != null) {
            // Traversal Left, Root, Right (l, R, r)
            inOrder(r.leftChild);
            System.out.print(r.data + ", ");
            inOrder(r.rightChild);
        }
    }

    public void preOrder(TreeNode r) {
        // If not empty
        if (r != null) {
            // Traversal Root, Left, Right (R, l, r)
            System.out.print(r.data  + ", ");
            preOrder(r.leftChild);
            preOrder(r.rightChild);
        }
    }

    public void postOrder(TreeNode r) {
        if (r != null) {
            // Traversal Left, Right, Root (l, r, R)
            postOrder(r.leftChild);
            postOrder(r.rightChild);
            System.out.print(r.data + ", ");
        }
    }

    public TreeNode searchNode(int d) {
        TreeNode aux=root;
        int c=0;
        while (aux.data!=d) {
            if (d<aux.data) {
                c++;
                aux=aux.leftChild;
            } else {
                c++;
                aux= aux.rightChild;
            }
            if ( aux == null ) {
                System.out.println("We've tried " + ( c + 1) + " times.");
                return null;
            }
        }
        System.out.println("Found at " + c + " step.");
        return aux;
    }

    public boolean deleteNode(int d) {
        TreeNode aux = root;
        TreeNode father = root;
        boolean isLeftChild = true;

        while (aux.data != d) {
            father = aux;
            if (d<aux.data) {
                isLeftChild = true;
                aux = aux.leftChild;
            } else {
                isLeftChild = false;
                aux = aux.rightChild;
            }
            if (aux == null) {
                return false;
            }
        }
        // --
        // Is leaf statement
        if (aux.leftChild == null && aux.rightChild == null) {
            if (aux == root) {
                root = null;
            } else if (isLeftChild) {
                father.leftChild = null;
            } else {
                father.rightChild = null;
            }
        } else if (aux.rightChild == null) {
            if (aux == root) {
                root = aux.leftChild;
            } else if (isLeftChild) {
                father.leftChild = aux.leftChild;
            } else {
                father.rightChild = aux.leftChild;
            }
        } else if (aux.leftChild == null) {
            if (aux == root) {
                root = aux.rightChild;
            } else if (isLeftChild) {
                father.leftChild = aux.rightChild;
            } else {
                father.rightChild = aux.rightChild;
            }
        } else {
            TreeNode replace = getReplaceNode(aux);
            if (aux==root) {
                root =replace;
            } else if (isLeftChild) {
                father.leftChild = replace;
            } else {
                father.rightChild = replace;
            }

            replace.leftChild = aux.leftChild;
        }
        return true;
    }

    // Get Node's replace
    public TreeNode getReplaceNode (TreeNode r ) {
        TreeNode reFather = r;
        TreeNode replace = r;
        TreeNode aux = r.rightChild;

        while (aux != null) {
            reFather = replace;
            replace = aux;
            aux = aux.leftChild;
        }

        if ( replace != r.rightChild ) {
            reFather.leftChild = replace.rightChild;
            replace.rightChild = r.rightChild;
        }
        System.out.println("Node's replacer: " + replace);
        return replace;
    }
}
